defmodule MiniCrm.CRMTest do
  use MiniCrm.DataCase

  alias MiniCrm.CRM

  describe "leads" do
    alias MiniCrm.CRM.Lead

    @valid_attrs %{city: "some city", company: "some company", country: "some country", department: "some department", email: "some email", first_name: "some first_name", last_name: "some last_name", phone: "some phone", status: "some status", street: "some street", title: "some title"}
    @update_attrs %{city: "some updated city", company: "some updated company", country: "some updated country", department: "some updated department", email: "some updated email", first_name: "some updated first_name", last_name: "some updated last_name", phone: "some updated phone", status: "some updated status", street: "some updated street", title: "some updated title"}
    @invalid_attrs %{city: nil, company: nil, country: nil, department: nil, email: nil, first_name: nil, last_name: nil, phone: nil, status: nil, street: nil, title: nil}

    def lead_fixture(attrs \\ %{}) do
      {:ok, lead} =
        attrs
        |> Enum.into(@valid_attrs)
        |> CRM.create_lead()

      lead
    end

    test "list_leads/0 returns all leads" do
      lead = lead_fixture()
      assert CRM.list_leads() == [lead]
    end

    test "get_lead!/1 returns the lead with given id" do
      lead = lead_fixture()
      assert CRM.get_lead!(lead.id) == lead
    end

    test "create_lead/1 with valid data creates a lead" do
      assert {:ok, %Lead{} = lead} = CRM.create_lead(@valid_attrs)
      assert lead.city == "some city"
      assert lead.company == "some company"
      assert lead.country == "some country"
      assert lead.department == "some department"
      assert lead.email == "some email"
      assert lead.first_name == "some first_name"
      assert lead.last_name == "some last_name"
      assert lead.phone == "some phone"
      assert lead.status == "some status"
      assert lead.street == "some street"
      assert lead.title == "some title"
    end

    test "create_lead/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CRM.create_lead(@invalid_attrs)
    end

    test "update_lead/2 with valid data updates the lead" do
      lead = lead_fixture()
      assert {:ok, %Lead{} = lead} = CRM.update_lead(lead, @update_attrs)
      assert lead.city == "some updated city"
      assert lead.company == "some updated company"
      assert lead.country == "some updated country"
      assert lead.department == "some updated department"
      assert lead.email == "some updated email"
      assert lead.first_name == "some updated first_name"
      assert lead.last_name == "some updated last_name"
      assert lead.phone == "some updated phone"
      assert lead.status == "some updated status"
      assert lead.street == "some updated street"
      assert lead.title == "some updated title"
    end

    test "update_lead/2 with invalid data returns error changeset" do
      lead = lead_fixture()
      assert {:error, %Ecto.Changeset{}} = CRM.update_lead(lead, @invalid_attrs)
      assert lead == CRM.get_lead!(lead.id)
    end

    test "delete_lead/1 deletes the lead" do
      lead = lead_fixture()
      assert {:ok, %Lead{}} = CRM.delete_lead(lead)
      assert_raise Ecto.NoResultsError, fn -> CRM.get_lead!(lead.id) end
    end

    test "change_lead/1 returns a lead changeset" do
      lead = lead_fixture()
      assert %Ecto.Changeset{} = CRM.change_lead(lead)
    end
  end

  describe "contacts" do
    alias MiniCrm.CRM.Contact

    @valid_attrs %{city: "some city", country: "some country", department: "some department", email: "some email", first_name: "some first_name", last_name: "some last_name", phone: "some phone", street: "some street", title: "some title"}
    @update_attrs %{city: "some updated city", country: "some updated country", department: "some updated department", email: "some updated email", first_name: "some updated first_name", last_name: "some updated last_name", phone: "some updated phone", street: "some updated street", title: "some updated title"}
    @invalid_attrs %{city: nil, country: nil, department: nil, email: nil, first_name: nil, last_name: nil, phone: nil, street: nil, title: nil}

    def contact_fixture(attrs \\ %{}) do
      {:ok, contact} =
        attrs
        |> Enum.into(@valid_attrs)
        |> CRM.create_contact()

      contact
    end

    test "list_contacts/0 returns all contacts" do
      contact = contact_fixture()
      assert CRM.list_contacts() == [contact]
    end

    test "get_contact!/1 returns the contact with given id" do
      contact = contact_fixture()
      assert CRM.get_contact!(contact.id) == contact
    end

    test "create_contact/1 with valid data creates a contact" do
      assert {:ok, %Contact{} = contact} = CRM.create_contact(@valid_attrs)
      assert contact.city == "some city"
      assert contact.country == "some country"
      assert contact.department == "some department"
      assert contact.email == "some email"
      assert contact.first_name == "some first_name"
      assert contact.last_name == "some last_name"
      assert contact.phone == "some phone"
      assert contact.street == "some street"
      assert contact.title == "some title"
    end

    test "create_contact/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CRM.create_contact(@invalid_attrs)
    end

    test "update_contact/2 with valid data updates the contact" do
      contact = contact_fixture()
      assert {:ok, %Contact{} = contact} = CRM.update_contact(contact, @update_attrs)
      assert contact.city == "some updated city"
      assert contact.country == "some updated country"
      assert contact.department == "some updated department"
      assert contact.email == "some updated email"
      assert contact.first_name == "some updated first_name"
      assert contact.last_name == "some updated last_name"
      assert contact.phone == "some updated phone"
      assert contact.street == "some updated street"
      assert contact.title == "some updated title"
    end

    test "update_contact/2 with invalid data returns error changeset" do
      contact = contact_fixture()
      assert {:error, %Ecto.Changeset{}} = CRM.update_contact(contact, @invalid_attrs)
      assert contact == CRM.get_contact!(contact.id)
    end

    test "delete_contact/1 deletes the contact" do
      contact = contact_fixture()
      assert {:ok, %Contact{}} = CRM.delete_contact(contact)
      assert_raise Ecto.NoResultsError, fn -> CRM.get_contact!(contact.id) end
    end

    test "change_contact/1 returns a contact changeset" do
      contact = contact_fixture()
      assert %Ecto.Changeset{} = CRM.change_contact(contact)
    end
  end
end
