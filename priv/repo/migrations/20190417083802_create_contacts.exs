defmodule MiniCrm.Repo.Migrations.CreateContacts do
  use Ecto.Migration

  def change do
    create table(:contacts) do
      add :first_name, :string
      add :last_name, :string
      add :email, :string
      add :phone, :string
      add :title, :string
      add :department, :string
      add :street, :text
      add :country, :string
      add :city, :string
      add :account_id, references(:accounts)

      timestamps()
    end

  end
end
