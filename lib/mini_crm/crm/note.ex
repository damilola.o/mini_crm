defmodule MiniCrm.CRM.CRM.Note do
  use Ecto.Schema
  import Ecto.Changeset
  alias MiniCrm.CRM.{Account, Contact, Lead, Opportunity}

  schema "notes" do
    field :body, :string
    many_to_many :accounts, Account, join_through: "accounts_notes"
    many_to_many :contacts, Contact, join_through: "contacts_notes"
    many_to_many :leads, Lead, join_through: "leads_notes"
    many_to_many :opportunities, Opportunity, join_through: "opportunities_notes"

    timestamps()
  end

  @doc false
  def changeset(note, attrs) do
    note
    |> cast(attrs, [:body])
    |> validate_required([:body])
  end
end
