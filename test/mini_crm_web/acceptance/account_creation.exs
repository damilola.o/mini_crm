defmodule MiniCrmWeb.Acceptance.AccountCreation do
  use MiniCrm.DataCase
  use Hound.Helpers

  hound_session()

  test "presence of create button" do
    navigate_to("/accounts")
    button = find_element(:class, "btn-custom") |> visible_text()
    assert button == "Create Account"
  end

  test "create button goes to new account creation page" do
    navigate_to("/accounts")
    find_element(:class, "btn-custom") |> click()
    assert current_path == "/accounts/new"
  end

  test "create a new account with valid data" do
    navigate_to("/accounts/new")

    form = find_element(:tag, "form")
    find_within_element(form, :name, "account[name]") |> fill_field("UBA")
    find_within_element(form, :name, "account[assigned_to]") |> fill_field("Isioma Queen")
    find_within_element(form, :name, "account[category]") |> fill_field("Sales")
    find_within_element(form, :name, "account[rating]") |> fill_field("1")
    find_within_element(form, :name, "account[tag]") |> fill_field("Upcoming")
    find_within_element(form, :name, "account[comment]") |> fill_field("Lorem ipsum blah")
    find_within_element(form, :name, "account[phone_number]") |> fill_field("+23412345678")
    find_within_element(form, :name, "account[website]") |> fill_field("www.help.com")
    find_within_element(form, :name, "account[email]") |> fill_field("test@email.com")
    find_within_element(form, :name, "account[address]") |> fill_field("3 Olaletan street")
    find_within_element(form, :name, "account[street1]") |> fill_field("street1")
    find_within_element(form, :name, "account[street2]") |> fill_field("street2")
    find_within_element(form, :name, "account[city]") |> fill_field("city")
    find_within_element(form, :name, "account[country]") |> fill_field("country")
    find_within_element(form, :name, "account[post_code]") |> fill_field("post_code")

    find_within_element(form, :css, "button") |> click

    assert current_path == "/accounts"
    message = find_element(:class, "alert") |> visible_text()

    assert message == "Account created successfully."
  end

  test "create a new account with invalid data" do
    navigate_to("/accounts/new")
    form = find_element(:tag, "form")
    find_within_element(form, :css, "button") |> click

    assert current_path == "/accounts"
    message = find_element(:class, "alert") |> visible_text()

    assert message == "Oops, some fields are empty"
  end
end
