defmodule MiniCrm.CRM.Account do
  use Ecto.Schema
  import Ecto.Changeset

  alias MiniCrm.CRM.{Opportunity, Contact, Note}

  schema "accounts" do
    field :address, :string
    field :assigned_to, :string
    field :category, :string
    field :city, :string
    field :comment, :string
    field :country, :string
    field :email, :string
    field :name, :string
    field :phone_number, :string
    field :post_code, :string
    field :rating, :integer
    field :street1, :string
    field :street2, :string
    field :tag, :string
    field :website, :string
    has_many :opportunities, Opportunity
    has_many :contacts, Contact
    many_to_many :notes, Note, join_through: "accounts_notes"

    timestamps()
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [:name, :assigned_to, :category, :rating, :tag, :comment, :phone_number, :website, :email, :address, :street1, :street2, :city, :country, :post_code])
    |> validate_required([:name, :assigned_to, :category, :rating, :tag, :comment, :phone_number, :website, :email, :address, :street1, :street2, :city, :country, :post_code])
  end
end
