defmodule MiniCrmWeb.OpportunityController do
  use MiniCrmWeb, :controller

  alias MiniCrm.CRM
  alias MiniCrm.CRM.Opportunity

  def index(conn, _params) do
    opportunities = CRM.list_opportunities()
    render(conn, "index.html", opportunities: opportunities)
  end

  def new(conn, _params) do
    changeset = CRM.change_opportunity(%Opportunity{})
    accounts = CRM.list_accounts() |> Enum.map(&{&1.name, &1.id})
    render(conn, "new.html", accounts: accounts, changeset: changeset)
  end

  def create(conn, %{"opportunity" => opportunity_params}) do
    account = CRM.get_opportunity_account(opportunity_params)
    case CRM.create_opportunity(account, opportunity_params) do
      {:ok, opportunity} ->
        conn
        |> put_flash(:info, "Opportunity created successfully.")
        |> redirect(to: Routes.opportunity_path(conn, :show, opportunity))

      {:error, %Ecto.Changeset{} = changeset} ->
        accounts = CRM.list_accounts() |> Enum.map(&{&1.name, &1.id})
        render(conn, "new.html", accounts: accounts, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    opportunity = CRM.get_opportunity!(id)
    render(conn, "show.html", opportunity: opportunity)
  end

  # def edit(conn, %{"id" => id}) do
  #   opportunity = CRM.get_opportunity!(id)
  #   account_id = Map.get(opportunity, "account_id")
  #   # account = CRM.get_account!(account_id)
  #   IO.inspect account_id
  #   accounts = CRM.get_opportunity_account(opportunity)
  #   # accounts = CRM.get_account() |> Enum.map(&{&1.name, &1.id})
  #   changeset = CRM.change_opportunity(opportunity)
  #   render(conn, "edit.html", opportunity: opportunity, changeset: changeset, accounts: accounts)
  # end

  # def update(conn, %{"id" => id, "opportunity" => opportunity_params}) do
  #   opportunity = CRM.get_opportunity!(id)

  #   case CRM.update_opportunity(opportunity, opportunity_params) do
  #     {:ok, opportunity} ->
  #       conn
  #       |> put_flash(:info, "Opportunity updated successfully.")
  #       |> redirect(to: Routes.opportunity_path(conn, :show, opportunity))

  #     {:error, %Ecto.Changeset{} = changeset} ->
  #       render(conn, "edit.html", opportunity: opportunity, changeset: changeset)
  #   end
  # end

  # def delete(conn, %{"id" => id}) do
  #   opportunity = CRM.get_opportunity!(id)
  #   {:ok, _opportunity} = CRM.delete_opportunity(opportunity)

  #   conn
  #   |> put_flash(:info, "Opportunity deleted successfully.")
  #   |> redirect(to: Routes.opportunity_path(conn, :index))
  # end

end
