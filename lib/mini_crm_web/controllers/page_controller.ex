defmodule MiniCrmWeb.PageController do
  use MiniCrmWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
