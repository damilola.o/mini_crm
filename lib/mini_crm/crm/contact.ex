defmodule MiniCrm.CRM.Contact do
  use Ecto.Schema
  import Ecto.Changeset
  alias MiniCrm.CRM.{Account, Note}

  schema "contacts" do
    field :city, :string
    field :country, :string
    field :department, :string
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :phone, :string
    field :street, :string
    field :title, :string
    belongs_to :accounts, Account, foreign_key: :account_id
    many_to_many :notes, Note, join_through: "contacts_notes"

    timestamps()
  end

  @doc false
  def changeset(contact, attrs) do
    contact
    |> cast(attrs, [:first_name, :last_name, :email, :phone, :title, :department, :street, :country, :city])
    |> validate_required([:first_name, :last_name, :email, :phone, :title, :department, :street, :country, :city])
  end
end
