defmodule MiniCrm.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :name, :string
      add :assigned_to, :string
      add :category, :string
      add :rating, :integer
      add :tag, :string
      add :comment, :text
      add :phone_number, :string
      add :website, :string
      add :email, :string
      add :address, :string
      add :street1, :string
      add :street2, :string
      add :city, :string
      add :country, :string
      add :post_code, :string

      timestamps()
    end

  end
end
