defmodule MiniCrm.Repo do
  use Ecto.Repo,
    otp_app: :mini_crm,
    adapter: Ecto.Adapters.Postgres
end
