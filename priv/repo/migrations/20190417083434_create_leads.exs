defmodule MiniCrm.Repo.Migrations.CreateLeads do
  use Ecto.Migration

  def change do
    create table(:leads) do
      add :first_name, :string
      add :last_name, :string
      add :email, :string
      add :phone, :string
      add :status, :string
      add :title, :string
      add :company, :string
      add :department, :string
      add :street, :text
      add :country, :string
      add :city, :string

      timestamps()
    end

  end
end
