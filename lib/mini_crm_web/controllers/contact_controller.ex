defmodule MiniCrmWeb.ContactController do
  use MiniCrmWeb, :controller

  alias MiniCrm.CRM
  alias MiniCrm.CRM.Contact

  def index(conn, _params) do
    contacts = CRM.list_contacts()
    render(conn, "index.html", contacts: contacts)
  end

  def new(conn, _params) do
    changeset = CRM.change_contact(%Contact{})
    accounts = CRM.list_accounts() |> Enum.map(&{&1.name, &1.id})
    countries = CRM.get_countries
    title = ["Mr", "Mrs", "Miss", "Dr.", "Prof."]
    render(conn, "new.html", changeset: changeset, accounts: accounts, title: title, countries: countries)
  end

  def create(conn, %{"contact" => contact_params}) do
    account = CRM.get_contact_account(contact_params)
    case CRM.create_contact(account, contact_params) do
      {:ok, contact} ->
        conn
        |> put_flash(:info, "Contact created successfully.")
        |> redirect(to: Routes.contact_path(conn, :show, contact))

      {:error, %Ecto.Changeset{} = changeset} ->
        accounts = CRM.list_accounts() |> Enum.map(&{&1.name, &1.id})
        countries = CRM.get_countries
        title = ["Mr", "Mrs", "Miss", "Dr.", "Prof."]
        render(conn, "new.html", changeset: changeset, accounts: accounts, title: title, countries: countries)
    end
  end

  def show(conn, %{"id" => id}) do
    contact = CRM.get_contact!(id)
    render(conn, "show.html", contact: contact)
  end

  def edit(conn, %{"id" => id}) do
    contact = CRM.get_contact!(id)
    changeset = CRM.change_contact(contact)
    render(conn, "edit.html", contact: contact, changeset: changeset)
  end

  def update(conn, %{"id" => id, "contact" => contact_params}) do
    contact = CRM.get_contact!(id)

    case CRM.update_contact(contact, contact_params) do
      {:ok, contact} ->
        conn
        |> put_flash(:info, "Contact updated successfully.")
        |> redirect(to: Routes.contact_path(conn, :show, contact))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", contact: contact, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    contact = CRM.get_contact!(id)
    {:ok, _contact} = CRM.delete_contact(contact)

    conn
    |> put_flash(:info, "Contact deleted successfully.")
    |> redirect(to: Routes.contact_path(conn, :index))
  end
end
