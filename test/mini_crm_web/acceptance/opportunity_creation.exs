defmodule MiniCrmWeb.Acceptance.OpportunityCreation do
  use MiniCrm.DataCase
  use Hound.Helpers

  hound_session()

  test "presence of create button" do
    navigate_to("/opportunities")
    button = find_element(:class, "btn-custom") |> visible_text()
    assert button == "Create Opportunity"
  end

  test "create button goes to new opportunity creation page" do
    navigate_to("/opportunities")
    find_element(:class, "btn-custom") |> click()
    assert current_path == "/opportunities/new"
  end

  test "create a new opportunity with valid data" do
    navigate_to("/opportunities/new")
    IO.inspect page_source()
    form = find_element(:tag, "form")
    find_within_element(form, :name, "opportunity[name]") |> fill_field("Something")
    find_within_element(form, :name, "opportunity[stage]") |> fill_field("Upcoming")
    find_within_element(form, :name, "opportunity[close_date]") |> fill_field("04/10/2019")
    find_within_element(form, :name, "opportunity[discount]") |> fill_field("1")
    find_within_element(form, :name, "opportunity[amount]") |> fill_field("1000")
    find_within_element(form, :name, "opportunity[probability]") |> fill_field("20")
    find_element(:css, "#opportunity_account_id option[value='UBA']") |> click
    find_within_element(form, :name, "opportunity[assigned_to]") |> fill_field("Chioma")
    find_within_element(form, :name, "opportunity[comment]") |> fill_field("Lorem ipsum")

    find_within_element(form, :css, "button") |> click

    assert current_path == "/opportunities"
    message = find_element(:class, "alert") |> visible_text()

    assert message == "Opportunity created successfully."
  end

end
