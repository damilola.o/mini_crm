defmodule MiniCrmWeb.LeadController do
  use MiniCrmWeb, :controller

  alias MiniCrm.CRM
  alias MiniCrm.CRM.Lead

  def index(conn, _params) do
    leads = CRM.list_leads()
    render(conn, "index.html", leads: leads)
  end

  def new(conn, _params) do
    changeset = CRM.change_lead(%Lead{})
    statuses = CRM.get_statuses
    countries = CRM.get_countries
    title = ["Mr", "Mrs", "Miss", "Dr.", "Prof."]
    render(conn, "new.html", changeset: changeset, statuses: statuses, countries: countries, title: title)
  end

  def create(conn, %{"lead" => lead_params}) do
    case CRM.create_lead(lead_params) do
      {:ok, lead} ->
        conn
        |> put_flash(:info, "Lead created successfully.")
        |> redirect(to: Routes.lead_path(conn, :show, lead))

      {:error, %Ecto.Changeset{} = changeset} ->
        statuses = CRM.get_statuses
        countries = CRM.get_countries
        title = ["Mr", "Mrs", "Miss", "Dr.", "Prof."]
        render(conn, "new.html", changeset: changeset, statuses: statuses, countries: countries, title: title)
    end
  end

  def show(conn, %{"id" => id}) do
    lead = CRM.get_lead!(id)
    changeset = CRM.change_lead(lead)
    accounts = CRM.list_accounts |> Enum.map(&{&1.name, &1.id})
    render(conn, "show.html", lead: lead, accounts: accounts, changeset: changeset)
  end

  def edit(conn, %{"id" => id}) do
    lead = CRM.get_lead!(id)
    changeset = CRM.change_lead(lead)
    render(conn, "edit.html", lead: lead, changeset: changeset)
  end

  def convert(conn, %{"id" => id}) do
    lead = CRM.get_lead!(id)
    {:ok, _lead} = CRM.delete_lead(lead)

    conn
    |> put_flash(:info, "Lead deleted successfully.")
    |> redirect(to: Routes.lead_path(conn, :index))
  end

  def update(conn, %{"id" => id, "lead" => lead_params}) do
    lead = CRM.get_lead!(id)
    status = %{"status" => "Converted"}
    case create_contact(lead, lead_params["account_id"]) do
      {:ok} ->
        case CRM.update_lead(lead, status) do
          {:ok, lead} ->
            conn
            |> put_flash(:info, "Lead successfully linked to account.")
            |> redirect(to: Routes.lead_path(conn, :index))

          {:error, %Ecto.Changeset{} = changeset} ->
            accounts = CRM.list_accounts |> Enum.map(&{&1.name, &1.id})
            conn
            |> put_flash(:info, "Error linking lead to account")
            |> render("show.html", lead: lead, accounts: accounts, changeset: %Ecto.Changeset{})
        end
      {:error} ->
        accounts = CRM.list_accounts |> Enum.map(&{&1.name, &1.id})
        conn
        |> put_flash(:info, "Error linking lead to account")
        |> render("show.html", lead: lead, accounts: accounts, changeset: %Ecto.Changeset{})
    end
  end

  def delete(conn, %{"id" => id}) do
    lead = CRM.get_lead!(id)
    {:ok, _lead} = CRM.delete_lead(lead)

    conn
    |> put_flash(:info, "Lead deleted successfully.")
    |> redirect(to: Routes.lead_path(conn, :index))
  end

  defp create_contact(lead, account_id) do
    contact_params = convert_lead_to_contact(lead, account_id)
    account = CRM.get_contact_account(contact_params)
    case CRM.create_contact(account, contact_params) do
      {:ok, contact} -> {:ok}
      {:error, %Ecto.Changeset{} = changeset} -> {:error}
    end
  end

  defp convert_lead_to_contact(lead, account_id) do
    contact_params = %{"account_id" => account_id, "city" => lead.city, "country" => lead.country,
      "department" => lead.department, "email" => lead.email, "first_name" => lead.first_name,
      "last_name" => lead.last_name, "phone" => lead.phone, "street" => lead.street, "title" => lead.title}
  end
end
