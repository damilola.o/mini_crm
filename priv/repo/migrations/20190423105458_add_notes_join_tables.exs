defmodule MiniCrm.Repo.Migrations.AddNotesJoinTables do
  use Ecto.Migration

  def change do
    create table(:accounts_notes) do
      add :account_id, references(:accounts)
      add :note_id, references(:notes)
    end
    create index(:accounts_notes, :account_id)
    create index(:accounts_notes, :note_id)


    create table(:contacts_notes) do
      add :contact_id, references(:contacts)
      add :note_id, references(:notes)
    end
    create index(:contacts_notes, :contact_id)
    create index(:contacts_notes, :note_id)

    create table(:leads_notes) do
      add :lead_id, references(:leads)
      add :note_id, references(:notes)
    end
    create index(:leads_notes, :lead_id)
    create index(:leads_notes, :note_id)

    create table(:opportunities_notes) do
      add :opportunity_id, references(:opportunities)
      add :note_id, references(:notes)
    end
    create index(:opportunities_notes, :opportunity_id)
    create index(:opportunities_notes, :note_id)
  end
end
