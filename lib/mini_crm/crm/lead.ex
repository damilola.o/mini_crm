defmodule MiniCrm.CRM.Lead do
  use Ecto.Schema
  import Ecto.Changeset
  alias MiniCrm.CRM.Note

  schema "leads" do
    field :city, :string
    field :company, :string
    field :country, :string
    field :department, :string
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :phone, :string
    field :status, :string
    field :street, :string
    field :title, :string
    many_to_many :notes, Note, join_through: "leads_notes"

    timestamps()
  end

  @doc false
  def changeset(lead, attrs) do
    lead
    |> cast(attrs, [:first_name, :last_name, :email, :phone, :status, :title, :company, :department, :street, :country, :city])
    |> validate_required([:first_name, :last_name, :email, :phone, :status, :title, :company, :department, :street, :country, :city])
  end
end
