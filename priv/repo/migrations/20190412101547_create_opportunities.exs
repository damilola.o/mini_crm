defmodule MiniCrm.Repo.Migrations.CreateOpportunities do
  use Ecto.Migration

  def change do
    create table(:opportunities) do
      add :name, :string
      add :stage, :string
      add :close_date, :date
      add :probability, :float
      add :amount, :decimal
      add :discount, :decimal
      add :assigned_to, :string
      add :comment, :text
      add :account_id, references(:accounts)

      timestamps()
    end

  end
end
