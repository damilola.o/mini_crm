defmodule MiniCrm.CRM.Opportunity do
  use Ecto.Schema
  import Ecto.Changeset

  alias MiniCrm.CRM.{Account, Note}

  schema "opportunities" do
    field :amount, :decimal
    field :assigned_to, :string
    field :close_date, :date
    field :comment, :string
    field :discount, :decimal
    field :name, :string
    field :probability, :float
    field :stage, :string
    belongs_to :accounts, Account, foreign_key: :account_id
    many_to_many :notes, Note, join_through: "opportunities_notes"

    timestamps()
  end

  @doc false
  def changeset(opportunity, attrs) do
    opportunity
    |> cast(attrs, [:name, :stage, :close_date, :probability, :amount, :discount, :assigned_to, :comment])
    |> validate_required([:name, :stage, :close_date, :probability, :amount, :discount, :assigned_to, :comment])
    |> foreign_key_constraint(:account_id)
  end
end
